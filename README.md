# API Challenge

Api challenge to test my backend development skills.

## Tech Stack

- [NestJS]
- [Postgress]
- [TypeOrm]
- [Docker]
- [Swagger]

## Api Docs

All essential documentation will be exposed at http:localhost:3000/api/docs once running the project in your local machine.

## Requirements

To run the project must have installed:

- Docker
- Git

## Installation and Start

```sh
git clone https://gitlab.com/Jhomiguel/api-challenge.git
cd api-challenge
docker-compose up
```

## Db Manager

For any direct transaction with the DB (force data reset), once running the project, PgAdmin will be exposed at http:localhost:5050.
All required credentials are avaible in the docker-compose file.

[nestjs]: https://docs.nestjs.com/
[postgress]: https://www.postgresql.org/
[typeorm]: https://typeorm.io/#/
[docker]: https://www.docker.com/
[swagger]: https://swagger.io/
