import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setUpSwagger } from './config/swagger.config';
import * as config from 'config';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const logger = new Logger('Bootstrap');
  const { port } = config.get('server');
  const app = await NestFactory.create(AppModule);
  setUpSwagger(app);
  await app.listen(port, () =>
    logger.log(`Application listening on port ${port}`),
  );
}
bootstrap();
