import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class GenerateTokenDto {
  @ApiPropertyOptional({
    description:
      'A random string to serve as the payload to sign the JWT token',
  })
  @IsNotEmpty()
  payload: string;
}
