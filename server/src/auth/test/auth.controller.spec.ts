import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import * as request from 'supertest';
import * as config from 'config';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import { JwtStrategy } from '../jwt.strategy';
import { INestApplication } from '@nestjs/common';

const jwtConfig = config.get('jwt');

describe('AuthController', () => {
  let controller: AuthController;
  let app: INestApplication;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
          secret: process.env.JWT_SECRET || jwtConfig.secret,
        }),
      ],
      controllers: [AuthController],
      providers: [AuthService, JwtStrategy],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    app = await module.createNestApplication().init();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return 400 if the body with the payload is not sent', () => {
    return request(app.getHttpServer())
      .post('/auth/generate-token')
      .expect(400);
  });

  it('should return 201 if the token was created successfully', () => {
    return request(app.getHttpServer())
      .post('/auth/generate-token')
      .send({ payload: 'testData' })
      .expect(201);
  });

  afterAll(async () => {
    await app.close();
  });
});
