import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from '../auth.service';
import { JwtStrategy } from '../jwt.strategy';
import * as config from 'config';

const jwtConfig = config.get('jwt');
describe('AuthService', () => {
  let authService: AuthService;
  let jwtService: JwtService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
          secret: process.env.JWT_SECRET || jwtConfig.secret,
        }),
      ],
      providers: [AuthService, JwtStrategy],
    }).compile();

    jwtService = module.get<JwtService>(JwtService);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('should generate a valid token if a proper payload is passed', () => {
    const { accessToken } = authService.generateAuthToken({ payload: 'data' });
    const isValid = jwtService.verify(accessToken, {
      secret: jwtConfig.secret,
    });
    expect(accessToken).toBeDefined();
    expect(isValid).toBeTruthy();
  });
});
