import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { GenerateTokenDto } from './dto/generate-token.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/generate-token')
  generateToken(@Body() generateTokenDto: GenerateTokenDto) {
    return this.authService.generateAuthToken(generateTokenDto);
  }
}
