import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { GenerateTokenDto } from './dto/generate-token.dto';

@Injectable()
export class AuthService {
  private logger = new Logger(AuthService.name);

  constructor(private jwtService: JwtService) {}

  generateAuthToken(generateTokenDto: GenerateTokenDto): {
    accessToken: string;
  } {
    try {
      this.logger.log('Generating jwt access token');
      const { payload } = generateTokenDto;
      const accessToken = this.jwtService.sign(payload);
      return { accessToken };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
