export const getMonthNumberByName = (monthName: string) => {
  monthName = monthName.toLowerCase();
  const months = [
    'january',
    'february',
    'march',
    'april',
    'may',
    'june',
    'july',
    'august',
    'september',
    'october',
    'november',
    'december',
  ];

  const dateNum = months.indexOf(monthName);
  return dateNum !== -1 ? dateNum + 1 : dateNum;
};
