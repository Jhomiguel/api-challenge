import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Timestamp,
} from 'typeorm';

@Entity()
export class Post extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('timestamp', { name: 'created_at', nullable: true })
  created_at: Timestamp;

  @Column({ nullable: true })
  title: string;

  @Column({ nullable: true })
  url: string;

  @Column({ nullable: true })
  author: string;

  @Column({ nullable: true })
  objectID: string;

  @Column({ nullable: true })
  points: number;

  @Column({ nullable: true })
  story_text: string;

  @Column({ nullable: true })
  comment_text: string;

  @Column({ nullable: true })
  num_comments: number;

  @Column({ nullable: true })
  story_id: number;

  @Column({ nullable: true })
  story_title: string;

  @Column({ nullable: true })
  story_url: string;

  @Column({ nullable: true })
  parent_id: number;

  @Column({
    nullable: true,
  })
  created_at_i: number;

  @Column('varchar', { array: true, nullable: true })
  _tags: string[];
}
