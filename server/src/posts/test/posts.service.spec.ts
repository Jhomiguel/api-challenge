import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { Post } from '../entities/post.entity';
import { PostsService } from '../posts.service';
import { mockedPostRepo } from './mocks/post.repository.mock';
import { mockedPostService } from './mocks/post.service.mock';

describe('PostsService', () => {
  let service: PostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
      providers: [
        {
          provide: getRepositoryToken(Post),
          useValue: mockedPostRepo,
        },
        {
          provide: PostsService,
          useValue: mockedPostService,
        },
      ],
    }).compile();

    service = module.get<PostsService>(PostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
