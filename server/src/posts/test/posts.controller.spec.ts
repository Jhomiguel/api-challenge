import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Post } from '../entities/post.entity';
import { PostsController } from '../posts.controller';
import { PostsService } from '../posts.service';
import { mockedPostRepo } from './mocks/post.repository.mock';
import { mockedPostService } from './mocks/post.service.mock';
import { AuthModule } from 'src/auth/auth.module';
import { posts } from './helpers/posts.dummy.data';

describe('PostsController', () => {
  let controller: PostsController;
  let app: INestApplication;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
      controllers: [PostsController],
      providers: [
        {
          provide: getRepositoryToken(Post),
          useValue: mockedPostRepo,
        },
        {
          provide: PostsService,
          useValue: mockedPostService,
        },
      ],
    }).compile();

    app = await module.createNestApplication().init();
    controller = module.get<PostsController>(PostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('Access token not supplied', () => {
    it('should return 401 (Forbidden) when trying to get all posts', () => {
      return request(app.getHttpServer()).get('/posts').expect(401);
    });

    it('should return 401 (Forbidden) when trying to get a single post by Id', () => {
      return request(app.getHttpServer()).get('/posts/1').expect(401);
    });

    it('should return 401 (Forbidden) when trying to delete a post by Id', () => {
      return request(app.getHttpServer()).delete('/posts/1').expect(401);
    });
  });

  describe('Access Token supplied', () => {
    let accessToken: string;
    beforeAll(async () => {
      const { body } = await request(app.getHttpServer())
        .post('/auth/generate-token')
        .send({ payload: 'testData' });

      accessToken = body.accessToken;
    });
    it('Should return 200 when trying to get a list of post', async () => {
      const res = await request(app.getHttpServer())
        .get('/posts')
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);
      expect(res.body).toStrictEqual(posts);
    });

    it('Should return 200 or 404 (if not found) when trying to get a single post by Id', async () => {
      await request(app.getHttpServer())
        .get('/posts/1')
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);

      const res = await request(app.getHttpServer())
        .get('/posts/50')
        .set('Authorization', 'Bearer ' + accessToken);

      expect(res.body).toMatchObject({
        statusCode: 404,
        message: 'Post with ID: "50" not found',
      });
    });

    it('Should return 200 or 404 (if not found) when trying to remove an existing post by Id', async () => {
      await request(app.getHttpServer())
        .delete('/posts/1')
        .set('Authorization', 'Bearer ' + accessToken)
        .expect(200);

      const res = await request(app.getHttpServer())
        .delete('/posts/60')
        .set('Authorization', 'Bearer ' + accessToken);

      expect(res.body).toMatchObject({
        statusCode: 404,
        message: 'Post with ID: "60" not found',
      });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
