import { posts } from '../helpers/posts.dummy.data';

const filteredPosts = [];

export const mockedPostRepo = {
  getPosts: jest.fn(() => Promise.resolve(posts)),
  createPosts: jest.fn(() => Promise.resolve(filteredPosts)),
};
