import { posts } from '../helpers/posts.dummy.data';

export const mockedPostService = {
  findAll: jest.fn(() => {
    return Promise.resolve(posts);
  }),
  findOne: jest.fn((id: number) => {
    return new Promise((resolve, reject) => {
      const foundPost = findPostById(id);
      foundPost
        ? resolve(foundPost)
        : reject({
            statusCode: 404,
            message: `Post with ID: "${id}" not found`,
          });
    });
  }),
  remove: jest.fn((id: number) => {
    return findPostById(id)
      ? Promise.resolve()
      : Promise.reject({
          statusCode: 404,
          message: `Post with ID: "${id}" not found`,
        });
  }),
};

const findPostById = (id: number) => posts.find((post) => post.id === id);
