import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ValidationPipe,
  ParseIntPipe,
  UseGuards,
  Post,
  HttpCode,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { UpdatePostDto } from './dto/update-post.dto';
import { GetPostFilterDto } from './dto/get-post-filter.dto';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Posts')
@ApiBearerAuth()
@ApiForbiddenResponse({ description: 'Must Provide an access token' })
@UseGuards(AuthGuard())
@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get()
  findAll(
    @Query(new ValidationPipe({ transform: true })) filterDto: GetPostFilterDto,
  ) {
    return this.postsService.findAll(filterDto);
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.postsService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updatePostDto: UpdatePostDto,
  ) {
    return this.postsService.update(id, updatePostDto);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.postsService.remove(id);
  }

  @ApiOkResponse({
    description: 'Force a reset of the data saved in the database',
  })
  @HttpCode(200)
  @Post('/reset')
  forceDataReset() {
    return this.postsService.forceDatareset();
  }
}
