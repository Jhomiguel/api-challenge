import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { UpdatePostDto } from './dto/update-post.dto';
import { GetPostFilterDto } from './dto/get-post-filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { PostRepository } from './posts.repository';
import axios from 'axios';
import * as config from 'config';

@Injectable()
export class PostsService {
  private readonly logger = new Logger(PostsService.name);
  private readonly apiConfig = config.get('api');

  constructor(
    @InjectRepository(PostRepository)
    private postRepository: PostRepository,
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  async savePostsFromApi() {
    try {
      this.logger.log('Getting the data from the API: `Hacker News`');
      const {
        data: { hits },
      } = await axios.get(this.apiConfig.url);

      this.postRepository.createPosts(hits);
    } catch (error) {
      this.logger.error(
        `Failed to get the data from the API: Hacker News`,
        error.stack,
      );
      throw new InternalServerErrorException(error);
    }
  }

  findAll(filter: GetPostFilterDto) {
    return this.postRepository.getPosts(filter);
  }

  async findOne(id: number) {
    const found = await this.postRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Post with ID: "${id}" not found`);
    }
    return found;
  }

  async update(id: number, updatePostDto: UpdatePostDto) {
    try {
      const post = await this.findOne(id);
      Object.assign(post, updatePostDto);
      return await post.save();
    } catch (error) {
      this.logger.error(
        `Failed to update the post with ID: ${id}. DtoParams: ${JSON.stringify(
          updatePostDto,
        )}`,
        error.stack,
      );
      throw new BadRequestException(error);
    }
  }

  async remove(id: number) {
    this.logger.log(`Removing the post with ID: ${id}`);
    const result = await this.postRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(`Post with ID: "${id}" not found`);
    }
  }

  async forceDatareset() {
    try {
      await this.postRepository.createQueryBuilder('posts').delete().execute();
      await this.savePostsFromApi();
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
