import { InternalServerErrorException, Logger } from '@nestjs/common';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { getMonthNumberByName } from 'src/utils/date.utils';
import { EntityRepository, Repository } from 'typeorm';
import { CreatePostDto } from './dto/create-post.dto';
import { GetPostFilterDto } from './dto/get-post-filter.dto';
import { Post } from './entities/post.entity';

@EntityRepository(Post)
export class PostRepository extends Repository<Post> {
  private logger = new Logger('PostRepository');

  async getPosts(
    getPostFilterDto: GetPostFilterDto,
  ): Promise<Pagination<Post>> {
    const query = this.createQueryBuilder('post');

    const { limit, page, ...postFilters } = getPostFilterDto;

    postFilters &&
      Object.keys(postFilters).forEach((param) => {
        if (postFilters[param]) {
          if (param === '_tags') {
            return query.andWhere(`post.${param} @> (:${param})`);
          }
          if (param === 'month') {
            return query.andWhere(
              `date_part('month',post.created_at) = :${param}`,
            );
          }
          query.andWhere(`post.${param} LIKE '%' || :${param} || '%'`);
        }
      });

    query.setParameters({
      ...postFilters,
      month: postFilters.month ? getMonthNumberByName(postFilters.month) : null,
      _tags: postFilters._tags?.split(','),
    });

    try {
      return await paginate<Post>(query, { limit, page });
    } catch (error) {
      this.logger.error(
        `Failed to get posts. Filters: ${JSON.stringify(getPostFilterDto)}"`,
        error.stack,
      );
      throw new InternalServerErrorException(error);
    }
  }

  async createPosts(posts: CreatePostDto[]): Promise<Post[]> {
    try {
      this.logger.log(
        'Saving the data from the API: `Hacker News` into the DB',
      );
      return await this.save(posts);
    } catch (error) {
      this.logger.error(
        `Failed to save the posts from the Hacker News API`,
        error.stack,
      );
      throw new InternalServerErrorException(error);
    }
  }
}
