import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';

export class GetPostFilterDto implements IPaginationOptions {
  @ApiPropertyOptional({ description: 'The author name' })
  @IsOptional()
  @IsNotEmpty()
  author: string;

  @ApiPropertyOptional({
    title: 'tags',
    description:
      'Tags must be separated by commas. Example: comment,story_28707764',
  })
  @IsOptional()
  @IsNotEmpty()
  _tags: string;

  @ApiPropertyOptional({
    description:
      'Can use keywords contained in the title to retrieve the post/s. Example: StatusCake',
  })
  @IsOptional()
  @IsNotEmpty()
  title: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  month: string;

  @ApiPropertyOptional({
    description:
      'Limit of the records that should be returned when making the API call',
    default: 5,
    required: false,
  })
  @IsOptional()
  @IsNotEmpty()
  limit = 5;

  @ApiPropertyOptional({
    description: 'Number of the current records page',
    default: 1,
    required: false,
  })
  @IsOptional()
  @IsNotEmpty()
  page = 1;
}
