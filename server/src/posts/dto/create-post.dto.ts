import { ApiPropertyOptional } from '@nestjs/swagger';

export class CreatePostDto {
  @ApiPropertyOptional()
  title: string;
  @ApiPropertyOptional()
  url: string;
  @ApiPropertyOptional()
  author: string;
  @ApiPropertyOptional()
  points: number;
  @ApiPropertyOptional()
  story_text: string;
  @ApiPropertyOptional()
  comment_text: string;
  @ApiPropertyOptional()
  num_comments: number;
  @ApiPropertyOptional()
  story_id: number;
  @ApiPropertyOptional()
  story_title: string;
  @ApiPropertyOptional()
  story_url: string;
  @ApiPropertyOptional()
  parent_id: number;
  @ApiPropertyOptional()
  _tags: string[];
}
