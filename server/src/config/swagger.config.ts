import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export function setUpSwagger(app: any) {
  const options = new DocumentBuilder()
    .setTitle('Api challenge')
    .setDescription('Api built in order to test my backend developer skills')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs', app, document);
}
